﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matematika
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задача Арифметика");

            int A, B, C, result;

            try
            {

                Console.Write("Введите А: ");
                A = int.Parse(Console.ReadLine());

                Console.Write("Введите B: ");
                B = int.Parse(Console.ReadLine());

                Console.Write("Введите C: ");
                C = int.Parse(Console.ReadLine());

                result = A * B;

                if ((A >= Math.Pow(10, 2)) || (B >= Math.Pow(10, 2)) || (C >= Math.Pow(10, 6)))
                {
                    Console.WriteLine("Неверно введены данные");
                }

                else if (result == C)
                {
                    Console.WriteLine("YES");

                }

                else if (result != C)
                {
                    Console.WriteLine("NO");
                }
            }
            catch
            {
                Console.WriteLine("Неверно введены данные");
            }

                Console.ReadKey();
        }
    }
}
